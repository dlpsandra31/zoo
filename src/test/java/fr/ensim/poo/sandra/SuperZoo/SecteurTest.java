package fr.ensim.poo.sandra.SuperZoo;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

public class SecteurTest {


	@Test
	public void testAjouterAnimal() throws AnimalDansMauvaisSecteurException {


		//CEci est un commentaire pour tester Git !
		Chat unChat = new Chat("Minou");
    	Chien unChien = new Chien("Grrr");
    	Chien deuxChien = new Chien("Graou");
    	
    	Secteur chiens = new Secteur("Chien");
    	
    	List<Secteur> lesSecteurs = new ArrayList<Secteur>();
    	lesSecteurs.add(chiens);
    	
    	Zoo monSuperZoo = new Zoo(40, lesSecteurs);

		if(unChien.getTypeAnimal() == chiens.obtenirType())
			chiens.ajouterAnimal(unChien);
		else
			throw new AnimalDansMauvaisSecteurException("Attention, cet animal ne va pas dans ce secteur");

    	if(deuxChien.getTypeAnimal() == chiens.obtenirType())
			chiens.ajouterAnimal(deuxChien);
		else
			throw new AnimalDansMauvaisSecteurException("Attention, cet animal ne va pas dans ce secteur");

		/*
		try {
    		monSuperZoo.nouveauVisiteur();
    		fail();
    	}
    	catch (LimiteVisiteurException limite){
    		System.out.println(limite.toString());
    	}
    	*/
	}

}
