package fr.ensim.poo.sandra.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import fr.ensim.poo.sandra.SuperZoo.Animal;
import fr.ensim.poo.sandra.SuperZoo.AnimalDansMauvaisSecteurException;
import fr.ensim.poo.sandra.SuperZoo.Chat;
import fr.ensim.poo.sandra.SuperZoo.Chien;
import fr.ensim.poo.sandra.SuperZoo.LimiteVisiteurException;
import fr.ensim.poo.sandra.SuperZoo.Secteur;
import fr.ensim.poo.sandra.SuperZoo.Zoo;

public class ZooTest {
	
	@Test
	public void testVisiteurs()
	{
		Secteur pingouins = new Secteur("Pingouin");
    	Secteur chiens = new Secteur("Chien");
    	Secteur chats = new Secteur("Chat");
    	
    	List<Secteur> lesSecteurs = new ArrayList<Secteur>();
    	lesSecteurs.add(pingouins);
    	lesSecteurs.add(chiens);
    	lesSecteurs.add(chats);
    	
    	Zoo monSuperZoo = new Zoo(45, lesSecteurs);
    	
    	Chat unChat = new Chat("Minou");
    	Chien unChien = new Chien("Grrr");
    	Animal unPingouin = new Animal("Damien", "Pingouin");
    	
    	/*
    	try {
    		monSuperZoo.nouveauVisiteur();
    		fail();
    	}
    	catch (LimiteVisiteurException limite){
    		System.out.println(limite.toString());
    	}
    	*/
    	
    	try {
			monSuperZoo.nouvelAnimal(unChat);
		} catch (AnimalDansMauvaisSecteurException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
	}
}
