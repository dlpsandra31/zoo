package fr.ensim.poo.sandra.SuperZoo;

public class LimiteVisiteurException extends Exception {
	
	public LimiteVisiteurException(String msg) {
		super(msg);
	}
}
