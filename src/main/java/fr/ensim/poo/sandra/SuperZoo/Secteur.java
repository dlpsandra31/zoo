package fr.ensim.poo.sandra.SuperZoo;

import java.util.ArrayList;
import java.util.List;

public class Secteur {
	private String typeAnimauxDansSecteur;
	private List<Animal> animauxDansSecteur;
	
	public Secteur(String leType)
	{
		this.animauxDansSecteur = new ArrayList<Animal>();
		this.typeAnimauxDansSecteur = leType;
	}
	
	public void ajouterAnimal(Animal lAnimal) throws AnimalDansMauvaisSecteurException
	{
		if(lAnimal.getTypeAnimal() == typeAnimauxDansSecteur)
			this.animauxDansSecteur.add(lAnimal);
		else
			throw new AnimalDansMauvaisSecteurException("Attention, cet animal ne va pas dans ce secteur");
	}
	
	public int getNombreAnimaux()
	{
		return this.animauxDansSecteur.size();
	}
	
	public String obtenirType()	{	return this.typeAnimauxDansSecteur; }
}
