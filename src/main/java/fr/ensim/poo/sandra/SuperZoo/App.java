package fr.ensim.poo.sandra.SuperZoo;

import java.util.ArrayList;
import java.util.List;

import fr.ensim.poo.sandra.SuperZoo.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App
{
	private static final Logger LOGGER = LogManager.getLogger(App.class.getName());

    public static void main( String[] args ) throws LimiteVisiteurException, AnimalDansMauvaisSecteurException {
		LOGGER.debug("Debug Message Logged !!!");
		LOGGER.info("Info Message Logged !!!");
		LOGGER.error("Error Message Logged !!!", new NullPointerException("NullError"));

    	Secteur pingouins = new Secteur("Pingouin");
    	Secteur chiens = new Secteur("Chien");
    	Secteur chats = new Secteur("Chat");
    	
    	List<Secteur> lesSecteurs = new ArrayList<Secteur>();
    	lesSecteurs.add(pingouins);
    	lesSecteurs.add(chiens);
    	lesSecteurs.add(chats);
    	
    	Zoo monSuperZoo = new Zoo(45, lesSecteurs);
    	
    	Chat unChat = new Chat("Minou");
    	Chien unChien = new Chien("Grrr");
    	Animal unPingouin = new Animal("Damien", "Pingouin");

    	monSuperZoo.nouvelAnimal(unChat);
    	monSuperZoo.nouvelAnimal(unChien);
    	monSuperZoo.nouvelAnimal(unPingouin);

		LOGGER.info("Nouvel animal : " + unChat.getClass().toString());
		LOGGER.trace(unChat);
		LOGGER.warn(unChat);
		LOGGER.fatal(unChat);
    }
}
