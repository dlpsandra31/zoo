package fr.ensim.poo.sandra.SuperZoo;

public class AnimalDansMauvaisSecteurException extends Exception {
	public AnimalDansMauvaisSecteurException(String msg) {
		super(msg);
	}
}
