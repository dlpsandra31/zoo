package fr.ensim.poo.sandra.SuperZoo;

import java.util.List;
import fr.ensim.poo.sandra.SuperZoo.Secteur;

public class Zoo {
	private int visiteurs;
	private List<Secteur> secteursAnimaux;
	
	public Zoo(int lesVisiteurs, List<Secteur> lesSecteurs)
	{
		this.visiteurs = lesVisiteurs;
		this.secteursAnimaux = lesSecteurs;
	}
	
	public void nouveauVisiteur() throws LimiteVisiteurException
	{
		if(this.visiteurs < this.getLimiteVisiteur())
			this.visiteurs++;
		else
			throw new LimiteVisiteurException("Le nombre de visiteurs est atteint !");
	}
	
	public int getLimiteVisiteur()
	{
		return secteursAnimaux.size()*15;
	}
	
	public void nouvelAnimal(Animal unAnimal) throws AnimalDansMauvaisSecteurException {
		int i = 0;
		while(i < secteursAnimaux.size() && secteursAnimaux.get(i).obtenirType() != unAnimal.getTypeAnimal())
		{
			i++;
		}
		this.secteursAnimaux.get(i).ajouterAnimal(unAnimal);
	}
	
	public int nombreAnimaux() {
		int nbAnimaux = 0;
		
		for(Secteur unSecteur : this.secteursAnimaux)
		{
			nbAnimaux += unSecteur.getNombreAnimaux();
		}
		
		return nbAnimaux;
	}
}
