package fr.ensim.poo.sandra.SuperZoo;

public class Animal {
	private String nomAnimal;
	private String typeAnimal;
	
	public Animal(String leNom, String leType)
	{
		this.nomAnimal = leNom;
		this.typeAnimal = leType;
	}
	
	public String getNomAnimal() {	return this.nomAnimal;	}
	
	public String getTypeAnimal() {	return this.typeAnimal; }
}
